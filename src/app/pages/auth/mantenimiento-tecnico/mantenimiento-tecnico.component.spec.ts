import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoTecnicoComponent } from './mantenimiento-tecnico.component';

describe('MantenimientoTecnicoComponent', () => {
  let component: MantenimientoTecnicoComponent;
  let fixture: ComponentFixture<MantenimientoTecnicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoTecnicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoTecnicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
