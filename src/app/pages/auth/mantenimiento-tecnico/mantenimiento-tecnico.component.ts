import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '@service'
import { NzNotificationService } from 'ng-zorro-antd/notification'
import { NzModalService } from 'ng-zorro-antd/modal'
import { DatePipe } from '@angular/common'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'app-mantenimiento-tecnico',
  templateUrl: './mantenimiento-tecnico.component.html',
  styleUrls: ['./mantenimiento-tecnico.component.scss'],
  providers: [DatePipe]
})
export class MantenimientoTecnicoComponent implements OnInit {

  public token_mantenimiento = ''
  public data: any
  public orden: any
  public items: any;

  public proveedor: any
  public tecnicos: any
  public idEntidad = 0

  public orden_detalle: Array<any> = [];
  public cronogramas: Array<any> = [];
  public load = false
  public response = false
  public response_type = 0;

  public data_mantenimiento: Array<any> = [];
  public data_protocolo: Array<any> = [];
  public fecha_man: Date;
  public equipo: any;

  constructor(
    private modal: NzModalService,
    private _DatePipe: DatePipe,
    private route: ActivatedRoute,
    private _ApiService: ApiService,
    private notification: NzNotificationService,
    public dialog: MatDialog,
  ) {
    this._ApiService.changeAuthTipo(1);
    this.fecha_man = new Date();
  }

  ngOnInit() {
    this.token_mantenimiento = this.route.snapshot.paramMap.get('token_mantenimiento')
    console.log('TOKEN ORDEN => ', this.token_mantenimiento)
    setTimeout(async () => {
      this.validarTokenOrden()
    }, 100)
  }

  public async validarTokenOrden() {
    try {
      const data: any = await this._ApiService._GET(`matenimiento/tecnico/${this.token_mantenimiento}`)
      if (data.data) {
        this.orden = data.data
        this.items = data.detalle

        if ( this.items.estado == 4 ) {
          this.items = null;
          this.orden = null;
          this.response = true
          this.response_type = 0
        } else {
          this.equipo = this.items.fkCronograma.fkEquipoInventario;
          this.idEntidad = data.id_entidad
          this.load = false
          this.response = false
          console.log('DATA ORDEN => ', data);
          const data_proto = JSON.parse(this.equipo.fkEquipo.detalle);
          if ( data_proto.length > 0 ) {
            this.data_protocolo = data_proto[0].value;
            console.log('DATA PROTOCOLO => ', this.data_protocolo );
          }
          await this.getParametro();
          await this.getproveedor();
        }
      } 
    } catch (error) {
      this.response = true
      this.response_type = 2
    }
  }

  public async getParametro() {
    try {
      const data: any = await this._ApiService._GET(`parametros/grupo/codigo/24/${this.items.fkCronograma.fkTipoMantenimiento}/${this.idEntidad}`);
      if ( data.codigo ) {
        const data_man: Array<any> = JSON.parse(data.nombreCorto);
        if ( data_man.length > 0 ) {
          this.data_mantenimiento = data_man;
          console.log('DATA_MANT => ', this.data_mantenimiento)
        }
        console.log('PARAMETRO => ', data)
      }
    } catch (error) {
    }
  }

  public async getproveedor() {
    const data = await this._ApiService._GET(
      `proveedor/tecnicos/fk/${this.orden.fkProveedor}/${this.idEntidad}`,
    )
    this.proveedor = data[0];
    this.tecnicos = data;
  }


  public validarInfo():boolean {
    let valid = true;
    this.data_mantenimiento.forEach( (item: any) => {
      if( item.value == null || item.value == "" ) {
        valid = false;
      }
    });

    if ( this.fecha_man == null ) {
      valid = false;
    }
    return valid;
  }

  public async guardar() {
    if ( this.validarInfo() ) {
      this.modal.confirm({
        nzTitle: '<i>Desea guardar la información?</i>',
        nzOnOk: async () => {
          const data_man = JSON.stringify(this.data_mantenimiento);
          try {
            this.load = true;
            const fecha_man = this._DatePipe.transform( this.fecha_man, 'yyy-MM-dd' );
            const data = await this._ApiService._POST(
              `mantenimiento/data/guardar/${fecha_man}/${this.token_mantenimiento}`, data_man
            )
            if ( data.data ) {
              this.orden = null;
              this.items = null
              this.response = true
              this.response_type = 1
              this.notification.success('Datos guardados!', 'La información del mantenimiento')
              this.load = false
            }
          } catch (error) {
            this.items = []
            this.load = false
            this.response = true
            this.response_type = 2
          }
        },
      })
    } else {
      this.notification.warning('Revisar', 'debe Completar los datos')
    }
  }

}
