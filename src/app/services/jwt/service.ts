import { Injectable, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import store from 'store'
import { Router } from '@angular/router'
import { Settings } from '../app.settings.model'
import { AppSettings } from '../app.settings'
import { NzModalService } from 'ng-zorro-antd'

@Injectable()
export class ApiService implements OnInit {
  public url: any
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })
  private httpHeaders2 = new HttpHeaders('multipart/*')
  public settings: Settings

  constructor(public appSettings: AppSettings, private http: HttpClient, private router: Router, private modalService: NzModalService) {
    this.getUrl()
    this.settings = this.appSettings.settings
  }

  ngOnInit(): void {}




  public agregarAuthorizationHeader2() {
    const token = localStorage.getItem('token')
    if (token != null && token !== '' && token !== undefined) {
    return this.httpHeaders2.append('Authorization', 'Bearer ' + token)
    } else {
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    this.router.navigate(['auth/login']);
    }
    return this.httpHeaders2
}


  public agregarAuthorizationHeader() {
    const token = localStorage.getItem('token')
    if (token != null && token !== '' && token !== undefined) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token)
    } else {
      localStorage.removeItem('token')
      localStorage.removeItem('usuario')
      this.router.navigate(['auth/login'])
    }
    return this.httpHeaders
  }

  public async getUrl() {
    const urlserver: any = await this.getEndPoints()
    this.url = urlserver
  }

  public async _GET(ruta: string): Promise<any> {
    const urlserver: any = await this.getEndPoints()
    const url = urlserver.auth
    const httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })
    return await this.http
      .get<any>(`${url}${ruta}`, { headers: httpHeaders })
      .toPromise()
  }

  public async _POST(ruta: string, body: any): Promise<any> {
    const urlserver: any = await this.getEndPoints()
    const url = urlserver.auth
    const httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })
    return await this.http
      .post<any>(`${url}${ruta}`, body, { headers: httpHeaders })
      .toPromise()
  }

  public async get(ruta: string): Promise<any> {
    return await this.http
      .get<any>(`${this.url.default}${ruta}`, { headers: this.agregarAuthorizationHeader() })
      .toPromise()
  }

  public post(ruta: string, body: any, multipart?) {
    let headers;
    if (multipart) {
       headers = this.agregarAuthorizationHeader2();
    } else {
        headers = this.agregarAuthorizationHeader();
    }
    console.log(headers);
    return this.http.post(`${this.url.default}${ruta}`, body, { headers: headers });
}


  public delete(ruta: string) {
    return this.http.delete(`${this.url.default}${ruta}`, {
      headers: this.agregarAuthorizationHeader(),
    })
  }

  public put(ruta: string, body: any) {
    return this.http.put(`${this.url.default}${ruta}`, body, {
      headers: this.agregarAuthorizationHeader(),
    })
  }

  public async getEndPoints(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const url = String(window.location.href).split('#')[0]
      this.http.get(`${url}assets/endpoints.json`).subscribe((data: any) => {
        resolve(data)
      })
    })
  }

  public async imprimirPDF(ruta: string) {
    // this.openSpinner();
    this.http
      .get(`${this.url.default}${ruta}`, {
        responseType: `arraybuffer`,
        headers: this.agregarAuthorizationHeader(),
      })
      .subscribe(
        (data: any) => {
          console.log('INFO => ', data)
          const file = new Blob([data], { type: `application/pdf` })
          const fileURL = URL.createObjectURL(file)
          window.open(fileURL)
          // this.closeSpinner();
        },
        error => {
          // this.closeSpinner();
          // this._AlertService.showError(`error al consultar el rerporte!`);
          console.log('error al consultar reporte')
        },
      )
  }

  public changeAuthTipo(tipo: number): void {
    this.settings.tipoAuth = tipo
  }

  public getAuthTipo(): number {
    return this.settings.tipoAuth
  }
}
