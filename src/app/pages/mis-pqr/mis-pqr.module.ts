import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { MisPqrRouterModule } from './mis-pqr-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/kit/widgets/widgets-components.module'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'
import { NgApexchartsModule } from 'ng-apexcharts'
import { MisPqrComponent } from './mis-pqr/mis-pqr.component'



const COMPONENTS = [
  MisPqrComponent,


]

@NgModule({
  imports: [
    SharedModule,
    MisPqrRouterModule,
    WidgetsComponentsModule,
    FormsModule,
    ChartistModule,
    NgApexchartsModule,
  ],
  declarations: [...COMPONENTS],
  entryComponents: [

  ]
})
export class MisPqrModule {}
