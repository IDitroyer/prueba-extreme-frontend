import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '@service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-tecnico-form',
  templateUrl: './tecnico-form.component.html',
  styleUrls: ['./tecnico-form.component.scss']
})
export class TecnicoFormComponent implements OnInit {

  public datos: FormGroup;
  public datoschanged: boolean = true;
  public load = false;
  public load_state = 0;
  public show = false;

  constructor(
    private modal: NzModalService,
    public formBuilder: FormBuilder,
    private service: ApiService,
    public dialogRef: MatDialogRef<TecnicoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private notification: NzNotificationService) {
    this.datos = this.formBuilder.group({
      idTecnico:  [0, Validators.required],
      documento: ['', Validators.compose([Validators.required, Validators.maxLength(10)])],
      email: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      estado:  [1, Validators.required],
      fkNivel:  [10, Validators.required],
      nombre: ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
      tarjeta: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
    });
    this.datos.valueChanges.subscribe(() => {
      this.datoschanged = true;
      let times: number = 0;
      let veces: number = 0;
      (<any>Object).values(this.datos.controls).forEach(control => {
        (<any>Object).values(this.datos.value).forEach(data => {
          if (veces === times) {
            if (control.value !== data) {
              this.datoschanged = false;
            }
          }
          veces++;
        });
        veces = 0;
        times++;
      });
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.show = true;
    }, 1500);
  }

  public setData() {
    const datos = this.datos.value;
    return {
      idTecnico: datos.idTecnico,
      documento: datos.documento,
      email: datos.email,
      estado: datos.estado,
      fkNivel: datos.fkNivel,
      nombre: datos.nombre,
      tarjeta: datos.tarjeta,
    }
  }

  public guardar() {
    try {
      this.load = true;
      const body = this.setData();
      this.load_state = 0;
      const data: any = this.service._POST(`proveedor/tecnicos/save/tecnico/${this.data.proveedor}/${this.data.entidad}`, body);
      if ( data.data ) {
        console.log('Tecnico => ', data);
        this.load_state = 1;
        this.notification.success('Datos guardados!', 'Orden digitada');
      } else {
        this.load_state = 2;
        this.notification.error('Error!', 'No se digito');        
      }
    } catch (error) {
      this.load_state = 2;
      this.notification.error('Error!', 'No se digito');
    }
  }

  public showConfirm(): void {
    if ( this.datos.valid === true ) {
      this.modal.confirm({
        nzTitle: '<i>Desea guardar los datos?</i>',
        nzOnOk: () => {
          this.guardar();
        },
      })
    } else {
      this.notification.warning('Revisar', 'Debe completar los información')
    }
  }

  public onNoClick(tipo: number): void {
    this.dialogRef.close(tipo)
  }

}
