import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { emailValidator, matchingPasswords, numberLimits } from 'src/app/components/validators/app-validators';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';



@Component({
  selector: 'app-usuarios-form',
  templateUrl: './usuarios-form.component.html',
  styleUrls: ['./usuarios-form.component.scss']
})
export class UsuariosFormComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public grupo: any;
  public usuario: any;
  

  constructor(private auth: AuthService, private notification: NzNotificationService, private modalService: NzModalService, private service: ApiService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<UsuariosFormComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.data.tipo === 1 ? this.isEdit = true : this.isEdit = false;
      this.usuario = this.data.usuario; 
      this.cargando = false;

      this.form = fb.group({
        nombre: ['', [Validators.required, Validators.minLength(6)]],
        identificacion: ['', [Validators.required,  numberLimits({min : 9, max : 11})]],
        correo: ['', [Validators.required, emailValidator]],
        usuario: ['', [Validators.required, Validators.minLength(6)]],
        celular: ['', [Validators.required, numberLimits({min : 6, max : 15}), ]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]],
      }, {validators :  matchingPasswords('password', 'confirmPassword')})


      if (this.usuario != null) {
        this.form.clearValidators();
        this.form.removeControl('password');
        this.form.removeControl('confirmPassword');        
        this.form.patchValue({...this.usuario})
      }
    }


 save() {

  if (this.form.valid) {

  this.modalService.confirm({
    nzTitle: `¿Desea guardar los cambios del usuario actual?`,
    nzOkText: 'Si',
    nzOkType: 'primary',
    nzOnOk: () => {
      console.log('sjdksd')
      if (this.isEdit) {
        this.editar();
      } else {
        this.crear();
      }
      
    },
    nzCancelText: 'No',
    nzOnCancel: () => console.log('Cancel'),
  });

  } else {
    this.notification.warning('Revise el formulario', 'El formulario contiene errores');
  }


 }



      crear() {
        this.form.clearValidators();
        this.form.removeControl('confirmPassword');       
        let body = {...this.form.value};
        

        this.service.post(`usuarios/crear`, body).subscribe(
          res => {this.cargando = false, this.onNoClick(1, res);},
          err => {this.cargando = false}
        )
      }

      editar() {
    
        let body = {...this.form.value};
      
        body.id = this.usuario.id;
        body.password = this.usuario.password;
        body.activo = this.usuario.activo;
        
        this.service.post(`usuarios/custom/save`, body).subscribe(
          res => {this.cargando = false, this.onNoClick(1, res);},
          err => {this.cargando = false}
        )

      }




    onNoClick(tipo: number, parametro : any): void {
      this.dialogRef.close({tipo : tipo, parametro : parametro});
    }
  ngOnInit(): void {


  }


}

