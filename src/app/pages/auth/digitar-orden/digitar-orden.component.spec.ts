/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { DebugElement } from '@angular/core'

import { DigitarOrdenComponent } from './digitar-orden.component'

describe('DigitarOrdenComponent', () => {
  let component: DigitarOrdenComponent
  let fixture: ComponentFixture<DigitarOrdenComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DigitarOrdenComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitarOrdenComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
