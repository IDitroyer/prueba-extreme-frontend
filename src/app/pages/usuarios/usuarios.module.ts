import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { UsuariosRouterModule } from './usuarios-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/kit/widgets/widgets-components.module'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'
import { NgApexchartsModule } from 'ng-apexcharts'
import { UsuariosComponent } from './usuarios/usuarios.component'
import { UsuariosFormComponent } from './usuarios/usuarios-form/usuarios-form.component'
import { AgregarPqrComponent } from './usuarios/pqrs/agregar-pqr/agregar-pqr.component'
import { PqrsComponent } from './usuarios/pqrs/pqrs.component'


const COMPONENTS = [
  UsuariosComponent,
  UsuariosFormComponent,
  PqrsComponent,
  AgregarPqrComponent

]

@NgModule({
  imports: [
    SharedModule,
    UsuariosRouterModule,
    WidgetsComponentsModule,
    FormsModule,
    ChartistModule,
    NgApexchartsModule,
  ],
  declarations: [...COMPONENTS],
  entryComponents: [

  ]
})
export class UsuariosModule {}
