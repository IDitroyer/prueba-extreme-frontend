
export const getMenuDataAdmin: any[] = [

  {
    category: true,
    title: 'Administrador',
  },
  {
    title: 'Usuarios y PQR',
    key: 'usuarios-pqr',
    icon: 'fe fe-users',
    url: '/usuarios',
  },

]



export const getMenuDataUser: any[] = [
  {
    category: true,
    title: 'Miembro usuario',
  },
  {
    title: 'Mis pqr',
    key: 'mis-pqr',
    icon: 'fe fe-help-circle',
    url: '/mis-pqr',
  },

]
