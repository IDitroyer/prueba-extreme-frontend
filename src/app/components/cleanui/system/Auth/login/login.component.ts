import { AfterViewInit, Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { select, Store } from '@ngrx/store'
import * as SettingsActions from 'src/app/store/settings/actions'
import { PublicService } from '@publicService'
import { AuthService } from '@authService'
import { NzNotificationService } from 'ng-zorro-antd'
import { Router } from '@angular/router';

declare let window: any; 



@Component({
  selector: 'cui-system-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  form: FormGroup
  logo: String
  authProvider: string = 'jwt'
  cargando: boolean = false
  entidades: any
  nit: string;

  constructor(
    private fb: FormBuilder,
    private notification: NzNotificationService,
    private store: Store<any>,
    private _PublicService: PublicService,
    private _AuthService: AuthService,
    private router: Router,
  ) {



    this.form = fb.group({
      userName: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required]],
    });
  }
  ngAfterViewInit(): void {


    }




  ngOnInit() {
    
  }

  public compare(o1, o2): boolean {
    return o1?.identificador == o2?.identificador;
  }




  get userName() {
    return this.form.controls.userName
  }
  get password() {
    return this.form.controls.password
  }

 

  submitForm(): void {
    this.userName.markAsDirty()
    this.userName.updateValueAndValidity()
    this.password.markAsDirty()
    this.password.updateValueAndValidity()
    if (this.userName.invalid || this.password.invalid ) {
      return
    }
    const payload = {      
      usuario: this.userName.value,
      password: this.password.value,
    }
    this.cargando = true
    this._AuthService.login(payload).subscribe(
      result => {
        this.cargando = false
        this.notification.success('Login exitoso', `Bienvenido`)
        this._AuthService.guardarToken(result.accessToken)      
        this.router.navigate(['usuarios']);
      },
      error => {
        console.log(error)
        this.cargando = false
      },
      () => {
        this.cargando = false
      },
    )
  }




  setProvider(authProvider) {
    this.store.dispatch(
      new SettingsActions.SetStateAction({
        authProvider,
      }),
    )
  }
}
