import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { MisPqrComponent } from './mis-pqr/mis-pqr.component'


const routes: Routes = [
  {
    path: 'mis-pqr',
    component:  MisPqrComponent,
    data: { title: 'MisPqr' },
  },

]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule],
})
export class MisPqrRouterModule {}
