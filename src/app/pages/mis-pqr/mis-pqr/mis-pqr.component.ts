import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NzModalService } from 'ng-zorro-antd';
import { ReportService } from '../../usuarios/reports.service';



@Component({
  selector: 'app-mis-pqr',
  templateUrl: './mis-pqr.component.html',
  styleUrls: ['./mis-pqr.component.scss']
})
export class MisPqrComponent implements OnInit {


  public cargando: boolean;

  public usuario: any;

  public pqrSearch: any;
  public cargandoTable: boolean;
  public pqrs: any;

  itemSelected: any;

  nombreEntidad: string;
  listOfData: any[] = []
  bordered = false
  loading = false
  sizeChanger = false
  pagination = true
  header = true
  title = true
  footer = true
  fixHeader = false
  size = 'small'
  expandable = false
  checkbox = true
  allChecked = false
  indeterminate = false
  displayData: any[] = []
  simple = false
  noResult = false
  position = 'bottom'
  isVisible = true
  

  constructor(private reportService: ReportService, private auth: AuthService, private modalService: NzModalService, private service: ApiService, public dialog: MatDialog,
     private fb: FormBuilder) {
     
    }


    checkAll(value: boolean): void {
      this.displayData.forEach(data => {
        if (!data.disabled) {
          data.checked = value
        }
      })
      this.refreshStatus(null)
    }


    currentPageDataChange(
      $event: Array<any>,
    ): void {
      this.displayData = $event
      this.refreshStatus(null)
    }
  
    refreshStatus(data): void {
  
  
      if(data) {
        this.displayData.forEach(d => {
          d.checked = false;
        })
  
        if (this.itemSelected && data.id === this.itemSelected.id) {
          this.displayData.filter(value => value.id === data.id)[0].checked = false;
          this.itemSelected = null
        } else {
          this.displayData.filter(value => value.id === data.id)[0].checked = true;
          this.itemSelected = this.displayData.filter(value => value.id === data.id)[0];
        }  
      } else {
        this.itemSelected = null;
      }

    }
    

    async verReporte() {

      this.modalService.confirm({
        nzTitle: `¿Desea generar el reporte de sus PQRs?`,
        nzOkText: 'Si',
        nzOkType: 'primary',
        nzOnOk: async () => {
  
          this.reportService.ReportePQR(this.pqrs);
  
  
        },
        nzCancelText: 'No',
        nzOnCancel: () => console.log('Cancel'),
      });
     
     
  
    }






  


    isExpired(pqr) {
      return new Date(pqr.fechaLimite).valueOf() < new Date().valueOf();
    }
  
  
  
    async getPqrs() {
      this.cargandoTable = true;
      this.pqrs = await this.service.get(`pqr/usuario`);
      this.cargandoTable = false;
      this.listOfData = this.pqrs;
      console.log(this.pqrs)
    }



  ngOnInit(): void {

    this.getPqrs();

  }


}

