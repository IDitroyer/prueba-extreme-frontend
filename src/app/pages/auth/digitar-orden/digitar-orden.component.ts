import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '@service'
import { NzNotificationService } from 'ng-zorro-antd/notification'
import { NzModalService } from 'ng-zorro-antd/modal'
import { DatePipe } from '@angular/common'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { TecnicoFormComponent } from './tecnico-form/tecnico-form.component'

@Component({
  selector: 'app-digitar-orden',
  templateUrl: './digitar-orden.component.html',
  styleUrls: ['./digitar-orden.component.scss'],
  providers: [DatePipe],
})
export class DigitarOrdenComponent implements OnInit {

  public token_orden = ''
  public data: any
  public orden: any
  public items: Array<any> = []

  public proveedor: any
  public tecnicos: any
  public idEntidad = 0

  public orden_detalle: Array<any> = [];
  public cronogramas: Array<any> = [];
  public load = false
  public response = false
  public response_type = 0

  constructor(
    private modal: NzModalService,
    private _DatePipe: DatePipe,
    private route: ActivatedRoute,
    private _ApiService: ApiService,
    private notification: NzNotificationService,
    public dialog: MatDialog,
  ) {
    this._ApiService.changeAuthTipo(1)
  }

  ngOnInit() {
    this.token_orden = this.route.snapshot.paramMap.get('token_orden')
    console.log('TOKEN ORDEN => ', this.token_orden)
    setTimeout(async () => {
      this.validarTokenOrden()
    }, 100)
  }

    // ABRIR FORMULARIO DE TECNICOS
    public openFormTecnicos() {
      this.modal.confirm({
        nzTitle: '<i>Desea agregar un tecnico?</i>',
        nzOnOk: () => {
          const dialogRef = this.dialog.open(TecnicoFormComponent, {
            width: '60%',
            height: 'auto',
            backdropClass: 'dark',
            data: { proveedor: this.orden.fkProveedor, entidad: this.idEntidad },
          })
          dialogRef.afterClosed().subscribe((res: any) => {
            switch (res) {
              case 1:
                this.getproveedor();
                break
            }
          })
        },
      })
    }

  public async validarTokenOrden() {
    try {
      const data: any = await this._ApiService._GET(`digitar/orden/${this.token_orden}`)
      if (data.data) {
        this.orden = data.data
        this.items = data.detalle
        this.idEntidad = data.id_entidad
        console.log('DATA ORDEN => ', data)
        await this.getproveedor()
      } else {
        if (data.not_valid == 0) {
          this.response = true
          this.response_type = 0
        }
      }
    } catch (error) {
      this.response = true
      this.response_type = 2
    }
  }

  public async getproveedor() {
    const data = await this._ApiService._GET(
      `proveedor/tecnicos/fk/${this.orden.fkProveedor}/${this.idEntidad}`,
    )
    this.proveedor = data[0]
    this.tecnicos = data
  }

  public async digitar() {
    if ( this.valirInfo() ) {
      for (let index = 0; index < this.items.length; index++) {
        let item = this.items[index];
        let fechaProveedor = this._DatePipe.transform(item.fkCronograma.fechaProveedor, 'yyy-MM-dd');
        let fechaProgramada = this._DatePipe.transform(item.fkCronograma.fechaProgramada, 'yyy-MM-dd');
        item.fkCronograma.fechaProveedor = `${fechaProveedor}T05:00:00.000+00:00`;
        item.fkCronograma.fechaProgramada = `${fechaProgramada}T05:00:00.000+00:00`;
        item.fkCronograma.valorCalibracion = item.fkCronograma.fkEquipoInventario.valorCalibracion;
        item.fkCronograma.valorMantenimiento = item.fkCronograma.fkEquipoInventario.costoPromMantenimiento;
      }
  
      const body = {
        orden: this.orden,
        ordenDetalle: this.items,
        cronogramas: [],
      }
      
      try {
        this.load = true
        const data: any = await this._ApiService._POST(`ordenes/digitar/${this.idEntidad}`, body)
        if (data.data) {
          this.items = []
          this.response = true
          this.response_type = 1
          console.log('ORDEN DGITADA => ', data)
          this.notification.success('Datos guardados!', 'Orden digitada')
          this.load = false
        } 
      } catch (error) {
        this.items = []
        this.load = false
        this.response = true
        this.response_type = 2
        this.notification.error('Error!', 'No se digito')
      }
    }
  }

  public valirInfo(): boolean {
    let valid: boolean = false;
    if ( this.items.length > 0 ) {
      for (let index = 0; index < this.items.length; index++) {
        const item = this.items[index];
        if (
          (item.fkCronograma.fkEquipoInventario.fkEquipo.codigo != null) &&
          (item.fkCronograma.fkEquipoInventario.fkEquipo.codigo != '') &&
          (item.fkCronograma.fkEquipoInventario.fkEquipo.nombre != null) &&
          (item.fkCronograma.fkEquipoInventario.fkEquipo.nombre != '') &&
          (item.fkCronograma.fechaProveedor != null) &&
          (item.fkCronograma.fechaProveedor != '') &&
          (item.fkCronograma.fechaProgramada != '') &&
          (item.fkCronograma.fechaProgramada != null) &&
          (item.fkCronograma.fkEquipoInventario.costoPromMantenimiento != null) &&
          (item.fkCronograma.fkEquipoInventario.costoPromCalibracion != null) &&
          (item.fkCronograma.horasParoEquipo != null) &&
          ( item.fkCronograma.fkProveedorTecnico != null ) 
        ) {
          valid = true;
        } else {
          this.notification.warning('Verificar!', 'Faltan datos por digitar');
          valid = false;
          break;
        }
      }
    } else {
      this.notification.warning('Verificar!', 'No hay información disponible');
    }
    return valid;
  }

  public showConfirm(): void {
    this.modal.confirm({
      nzTitle: '<i>Desea digitar la orden de trabajo?</i>',
      nzOnOk: () => {
        this.digitar()
      },
    })
  }

}
