import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NzModalService } from 'ng-zorro-antd';
import { AgregarPqrComponent } from './agregar-pqr/agregar-pqr.component';
import { ReportService } from '../../reports.service';



@Component({
  selector: 'app-pqrs',
  templateUrl: './pqrs.component.html',
  styleUrls: ['./pqrs.component.scss']
})
export class PqrsComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public grupo: any;
  public usuario: any;

  public pagoSearch: any;

  public pqrs: any;

  public cargandoTable: boolean;
 

  itemSelected: any;

  nombreEntidad: string;
  listOfData: any[] = []
  bordered = false
  loading = false
  sizeChanger = false
  pagination = true
  header = true
  title = true
  footer = true
  fixHeader = false
  size = 'small'
  expandable = false
  checkbox = true
  allChecked = false
  indeterminate = false
  displayData: any[] = []
  simple = false
  noResult = false
  position = 'bottom'
  isVisible = true


  constructor(private reportService: ReportService, private auth: AuthService, private modalService: NzModalService, private service: ApiService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<PqrsComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.usuario = this.data.usuario;

  }


  checkAll(value: boolean): void {
    this.displayData.forEach(data => {
      if (!data.disabled) {
        data.checked = value
      }
    })
    this.refreshStatus(null)
  }


  currentPageDataChange(
    $event: Array<any>,
  ): void {
    this.displayData = $event
    this.refreshStatus(null)
  }

  refreshStatus(data): void {
    if (data) {
      this.displayData.forEach(d => {
        d.checked = false;
      })
      if (this.itemSelected && data.id === this.itemSelected.id) {
        this.displayData.filter(value => value.id === data.id)[0].checked = false;
        this.itemSelected = null
      } else {
        this.displayData.filter(value => value.id === data.id)[0].checked = true;
        this.itemSelected = this.displayData.filter(value => value.id === data.id)[0];
      }
    } else {
      this.itemSelected = null;
    }
  }

  async verReporte() {

    this.modalService.confirm({
      nzTitle: `¿Desea generar el reporte de PQRs de usuario ${this.usuario.nombre}?`,
      nzOkText: 'Si',
      nzOkType: 'primary',
      nzOnOk: async () => {

        this.reportService.ReportePQR(this.pqrs);


      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });
   
   

  }

  pqrForm(tipo) {
    const dialogRef = this.dialog.open(AgregarPqrComponent, {
      width: '40%',
      data: { tipo : tipo, usuario: this.usuario, pqr: this.itemSelected }
    });
    dialogRef.afterClosed().subscribe(res => {
      switch (res.tipo) {
        case 1:
          this.getPqrs();
          break;
      }
    });
  }


  cambiarEstado(estado) {
    this.modalService.confirm({
      nzTitle: `¿Desea cambiar el estado del pqr actual?`,
      nzOkText: 'Si',
      nzOkType: 'primary',
      nzOnOk: () => {
        const old = this.itemSelected.estado;
        this.itemSelected.estado = estado;
        this.service.post(`pqr/save`, this.itemSelected).subscribe(
          res => { },
          err => { this.itemSelected.estado = old}
        )
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });




   
  }


  eliminarPqr(id) {


    this.modalService.confirm({
      nzTitle: `¿Desea eliminar el pqr actual?`,
      nzOkText: 'Si',
      nzOkType: 'primary',
      nzOnOk: () => {


        this.service.delete(`pqr/delete/${id}`).subscribe(
          res => { this.getPqrs(); },
          err => { }
        )

      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });


  }

  isExpired(pqr) {
    return new Date(pqr.fechaLimite).valueOf() < new Date().valueOf();
  }



  async getPqrs() {
    this.cargandoTable = true;
    this.pqrs = await this.service.get(`pqr/usuario/${this.usuario.id}`);
    this.cargandoTable = false;
    this.listOfData = this.pqrs;
    console.log(this.pqrs)
  }


  onNoClick(tipo: number): void {
    this.dialogRef.close(tipo);
  }
  ngOnInit(): void {

    this.getPqrs();

  }


}

