import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { AuthRouterModule } from './auth-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SystemModule } from 'src/app/components/cleanui/system/system.module'

// system pages
import { LoginPage } from './login/login.component'
import { RegisterPage } from './register/register.component'
import { LockscreenPage } from './lockscreen/lockscreen.component'
import { ForgotPasswordPage } from './forgot-password/forgot-password.component'
import { Error500Page } from './500/500.component'
import { Error404Page } from './404/404.component'
import { DigitarOrdenComponent } from './digitar-orden/digitar-orden.component'
import { CalendarModule, DateAdapter } from 'angular-calendar'
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { TecnicoFormComponent } from './digitar-orden/tecnico-form/tecnico-form.component';
import { MantenimientoTecnicoComponent } from './mantenimiento-tecnico/mantenimiento-tecnico.component'

import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon';

const COMPONENTS = [
  LoginPage,
  RegisterPage,
  LockscreenPage,
  ForgotPasswordPage,
  Error500Page,
  Error404Page,
  DigitarOrdenComponent,
  TecnicoFormComponent,
  MantenimientoTecnicoComponent,
]

@NgModule({
  imports: [
    SharedModule,
    AuthRouterModule,
    FormsModule,
    ReactiveFormsModule,
    SystemModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MatButtonModule,
    MatIconModule
  ],
  declarations: [...COMPONENTS ],
  entryComponents: [
    TecnicoFormComponent,
  ]
})
export class AuthModule {}
