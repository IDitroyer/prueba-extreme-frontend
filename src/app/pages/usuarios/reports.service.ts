import { Injectable, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import store from 'store'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from "pdfmake/build/vfs_fonts";



@Injectable()
export class ReportService implements OnInit {
public url: any;
private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });

constructor(private http: HttpClient) {}
    ngOnInit(): void {
    }


    makeCell(content, rowIndex = -1, options = {}) {
        return Object.assign({text: content, fillColor: rowIndex % 2 ? 'white' : '#e8e8e8'}, options);
    }
    
    
    thl(content, rowIndex = -1, options = {}) {
        return this.makeCell(content, rowIndex, Object.assign({ bold: true, alignment: 'left', fontSize: 9 }, options));
    }


    thr(content, rowIndex = -1, options = {}) {
        return this.makeCell(content, rowIndex, Object.assign({ bold: true, alignment: 'right', fontSize: 9 }, options));
    }
    tdl(content, rowIndex = -1, options = {}) {
        return this.makeCell( content, rowIndex, Object.assign({ bold: false, alignment: 'left', fontSize: 9 }, options));
    }
    tdr(content, rowIndex = -1, options = {}) {
        return this.makeCell( content, rowIndex,Object.assign({ bold: false, alignment: 'right', fontSize: 9 }, options));
    }
    

    reportDate = () => new Date().toLocaleString();
    
    
    truncateContent(content, maxLength = 17){
        return ''.concat(content.slice(0, maxLength), content.length > maxLength ? '…' : '');
    }
    
    
    
    createDocumentDefinition(reportDate, subHeading, contentParts){
        const baseDocDefinition = {
            pageSize: 'A4',
            footer: (currentPage, pageCount) => {
                return {
                    text: `${reportDate} : Pagina ${currentPage.toString()} de ${pageCount.toString()}`,
                    alignment: 'center',
                    fontSize: 7
                }
            },
    
            styles: {
                title: {
                    fontSize: 24
                },
                titleSub: {
                    fontSize: 18
                },
                titleDate: {
                    fontSize: 14,
                    alignment: 'right',
                    bold: true
                }
            },
    
            content: [
                {
                    columns: [
                        {text: 'Extreme technologies ', style: 'title', width: '*'},
                        {text: reportDate, style: 'titleDate', width: '160'},
                    ]
                },
                {text: `${subHeading}\n\n`, style: 'titleSub'},
            ],
        };
        const docDefinition = JSON.parse(JSON.stringify(baseDocDefinition));
        docDefinition.footer = baseDocDefinition.footer;
        docDefinition.content.push(contentParts);
        return docDefinition;
    };
    
    
    
    
    
    
ReportePQR(data) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;

        
        const tableBody = (dataRows) => {
            const body = [
                [
                    this.thl('Datos del usuario', -1, {colSpan: 2}),
                    this.thl(' '),
                    this.thr('tipo'),
                    this.thr('asunto'),
                    this.thr('fecha creacion'),
                    this.thr('fecha expiracion'),
                    this.thr('estado')
                ],
                []
            ];
    
    
    
            dataRows.forEach((row, index) => {
                const tableRow = [];
                console.log(row)
                tableRow.push(this.tdl(row.idUsuario.usuario, index));
                tableRow.push(this.tdl(row.idUsuario.identificacion, index));
                
                let tipo = '';
                let estado = '';
                switch (row.estado) {
                    case 1:
                        estado = 'Nuevo'
                        break;
                    case 2:
                        estado = 'En proceso'
                        break;
                    case 3:
                        estado = 'Cerrado'
                        break;                    
                }



                switch (row.tipo) {
                    case 1:
                        tipo = 'Petición'
                        break;
                    case 2:
                        tipo = 'Queja'
                        break;
                    case 3:
                        tipo = 'Reclamo'
                        break;                    
                }



                tableRow.push(this.tdr(tipo, index));
                tableRow.push(this.tdr(this.truncateContent(row.asunto, 20), index));
                tableRow.push(this.tdr(new Date(row.fechaCreacion).toLocaleString(), index));
                tableRow.push(this.tdr(new Date(row.fechaLimite).toLocaleString(), index));
                tableRow.push(this.tdr(estado, index));
                body.push(tableRow);
    
         
            });
            body[1] = [
                this.tdl(`Total datos ${data.length}`, -1, {colSpan: 7, fillColor: 'black', color: 'white'}),
             
            ];
            return body;
        }
    
        const tableData = {
            table: {
                
                headerRows: 1,
                widths: ['*', 70, 70, 70, 70, 70, 70],
    
                body: tableBody(data),
            }
        };
        const docDefinition = this.createDocumentDefinition(this.reportDate(), 'Reporte de PQRs', tableData);
        return pdfMake.createPdf(docDefinition).download(`pqr_report_${this.reportDate()}.pdf`);
    }
    






}






